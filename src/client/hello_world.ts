/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */

import {
  Keypair,
  Connection,
  PublicKey,
  LAMPORTS_PER_SOL,
  SystemProgram,
  TransactionInstruction,
  Transaction,
  sendAndConfirmTransaction,
} from '@solana/web3.js';
import fs from 'mz/fs';
import path from 'path';
import * as borsh from 'borsh';

import {getPayer, getRpcUrl, createKeypairFromFile} from './utils';

/**
 * Connection to the network
 */
let connection: Connection;

/**
 * Keypair associated to the fees' user_account
 */
let user_account: Keypair;

let programKeypair: Keypair;

/**
 * Hello world's program id
 */
let programId: PublicKey;

/**
 * The public key of the account we are saying hello to
 */
let greetedPubkey: PublicKey;

/**
 * Path to program files
 */
const PROGRAM_PATH = path.resolve(__dirname, '../../dist/program');

/**
 * Path to program shared object file which should be deployed on chain.
 * This file is created when running either:
 *   - `npm run build:program-c`
 *   - `npm run build:program-rust`
 */
const PROGRAM_SO_PATH = path.join(PROGRAM_PATH, 'helloworld.so');

/**
 * Path to the keypair of the deployed program.
 * This file is created when running `solana program deploy dist/program/helloworld.so`
 */
const PROGRAM_KEYPAIR_PATH = path.join(PROGRAM_PATH, 'helloworld-keypair.json');

let pda_account: PublicKey;

let bump_seed: number;

let seeds: [Buffer];

let system_account: PublicKey;

let rent_account: PublicKey;

/**
 * The state of a greeting account managed by the hello world program
 */
class GreetingAccount {
  counter = 0;
  constructor(fields: {counter: number} | undefined = undefined) {
    if (fields) {
      this.counter = fields.counter;
    }
  }
}

/**
 * Borsh schema definition for greeting accounts
 */
const GreetingSchema = new Map([
  [GreetingAccount, {kind: 'struct', fields: [['counter', 'u32']]}],
]);

/**
 * The expected size of each greeting account.
 */
const GREETING_SIZE = borsh.serialize(
  GreetingSchema,
  new GreetingAccount(),
).length;

/**
 * Establish a connection to the cluster
 */
export async function establishConnection(): Promise<void> {
  const rpcUrl = await getRpcUrl();
  connection = new Connection(rpcUrl, 'confirmed');
  const version = await connection.getVersion();
  console.log('Connection to cluster established:', rpcUrl, version);
}

/**
 * Establish an account to pay for everything
 */
export async function establishPayer(): Promise<void> {
  let fees = 0;
  if (!user_account) {
    const {feeCalculator} = await connection.getRecentBlockhash();

    // Calculate the cost to fund the greeter account
    fees += await connection.getMinimumBalanceForRentExemption(GREETING_SIZE);

    // Calculate the cost of sending transactions
    fees += feeCalculator.lamportsPerSignature * 100; // wag

    user_account = await getPayer();
  }

  let lamports = await connection.getBalance(user_account.publicKey);
  if (lamports < fees) {
    // If current balance is not enough to pay for fees, request an airdrop
    const sig = await connection.requestAirdrop(
      user_account.publicKey,
      fees - lamports,
    );
    await connection.confirmTransaction(sig);
    lamports = await connection.getBalance(user_account.publicKey);
  }

  console.log(
    'Using account',
    user_account.publicKey.toBase58(),
    'containing',
    lamports / LAMPORTS_PER_SOL,
    'SOL to pay for fees',
  );
}

/**
 * Check if the hello world BPF program has been deployed
 */
export async function checkProgram(): Promise<void> {
  // Read program id from keypair file
  try {
    programKeypair = await createKeypairFromFile(PROGRAM_KEYPAIR_PATH);
    programId = programKeypair.publicKey;
    console.log('programId:', programId.toBase58());
    system_account = new PublicKey('11111111111111111111111111111111');
    console.log('system_account:', system_account.toBase58());
    rent_account = new PublicKey('SysvarRent111111111111111111111111111111111');
    console.log('rent_account:', rent_account.toBase58());
  } catch (err) {
    const errMsg = (err as Error).message;
    throw new Error(
      `Failed to read program keypair at '${PROGRAM_KEYPAIR_PATH}' due to error: ${errMsg}. Program may need to be deployed with \`solana program deploy dist/program/helloworld.so\``,
    );
  }
}

export async function createPdaAccount(): Promise<void> {
  const signers = [user_account];
  const instruction = new TransactionInstruction({
    keys: [
      {pubkey: user_account.publicKey, isSigner: false, isWritable: true},
      {pubkey: pda_account, isSigner: false, isWritable: true},
      {pubkey: system_account, isSigner: false, isWritable: true},
      {pubkey: programId, isSigner: false, isWritable: true},
      {pubkey: rent_account, isSigner: false, isWritable: true},
    ],
    programId,
    data: Buffer.from('0'), // All instructions are hellos
  });
  await sendAndConfirmTransaction(
    connection,
    new Transaction().add(instruction),
    signers,
  ).then(function(res) { console.log(res) } );
}

export async function findPdaAddress(): Promise<void> {
  console.log('programId HERE:', programId.toBase58());
  seeds = [ Buffer.from( 'P@y$tk6#n8KV2NE^#WZXvU4O9%qRhiqd' ) ];
  [pda_account, bump_seed] = await PublicKey.findProgramAddress(seeds, programId);
  console.log('pda_account:', pda_account.toBase58());
}

export async function deposit(): Promise<void> {
  console.log('Transferring funds');
  console.log("fromPubkey:", user_account.publicKey.toBase58());
  console.log("toPubkey:", pda_account.toBase58());
  const signers = [user_account];
  const lamports = 1000000;
  const transaction = new Transaction().add(
    SystemProgram.transfer({
      fromPubkey: user_account.publicKey,
      toPubkey: pda_account,
      lamports,
    }),
  );
  await sendAndConfirmTransaction(connection, transaction, signers).then(function(res) {
    console.log(res)
  });
}

export async function unlock(): Promise<void> {
  console.log('Withdrawing funds');
  console.log("pda_account:", pda_account.toBase58());
  console.log("user_account:", user_account.publicKey.toBase58());
  const signers = [user_account, programKeypair];
  const instruction = new TransactionInstruction({
    keys: [
      {pubkey: pda_account, isSigner: false, isWritable: true},
      {pubkey: user_account.publicKey, isSigner: false, isWritable: true},
    ],
    programId,
    data: Buffer.from("1"),
  });

  await sendAndConfirmTransaction(
    connection,
    new Transaction().add(instruction),
    signers,
  ).then(function(res) {
    console.log(res)
  });
}
