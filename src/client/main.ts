/**
 * Hello world
 */

import {
  establishConnection,
  establishPayer,
  checkProgram,
  findPdaAddress,
  createPdaAccount,
  deposit,
  unlock,
} from './hello_world';

async function main() {
  // Establish connection to the cluster
  await establishConnection();

  // Determine who pays for the fees
  await establishPayer();

  // Check if the program has been deployed
  await checkProgram();

  // Find the program address you will use
  await findPdaAddress();

  // Set owner of pda account
  // Note: will fail if acount already exists
  await createPdaAccount();
  
  // unlock funds
  await unlock();

  console.log('Success');
}

main().then(
  () => process.exit(),
  err => {
    console.error(err);
    process.exit(-1);
  },
);
