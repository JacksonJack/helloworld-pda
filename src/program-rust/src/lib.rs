use borsh::{BorshDeserialize, BorshSerialize};
use solana_program::{
    account_info::{next_account_info, AccountInfo},
    entrypoint,
    entrypoint::ProgramResult,
    msg,
    program::{invoke, invoke_signed},
    program_error::ProgramError,
    pubkey::Pubkey,
    rent::Rent,
    system_instruction,
    sysvar::Sysvar
};
use std::str;

/// Define the type of state stored in accounts
#[derive(BorshSerialize, BorshDeserialize, Debug)]
pub struct GreetingAccount {
    /// number of greetings
    pub counter: u32,
}

// Declare and export the program's entrypoint
entrypoint!(process_instruction);

// Program entrypoint's implementation
pub fn process_instruction(
    program_id: &Pubkey, // Public key of the account the hello world program was loaded into
    accounts: &[AccountInfo], // The account to say hello to
    instruction_data: &[u8], // Ignored, all helloworld instructions are hellos
) -> ProgramResult {
    msg!("helloworld-pda entrypoint");

    let my_instruction_str = str::from_utf8(&instruction_data).unwrap();
    msg!("my_instruction_str: {}", my_instruction_str);

    match my_instruction_str {
        "0" => process_create_pda_account(program_id, accounts),
        "1" => prcoess_unlock_funds(program_id, accounts),
        &_ => {
          msg!("do_nothing");
          Ok(())
        },
    }
}

fn process_create_pda_account(
  program_id: &Pubkey,
  accounts: &[AccountInfo], 
) -> ProgramResult {
    msg!("process_create_pda_account");

    let account_info_iter = &mut accounts.iter();

    let funder_info = next_account_info(account_info_iter)?; //
    let associated_token_account_info = next_account_info(account_info_iter)?; //
    let system_program_info = next_account_info(account_info_iter)?; //
    let this_program_info = next_account_info(account_info_iter)?;
    let this_program_id = this_program_info.key; //
    let rent = &Rent::from_account_info(next_account_info(account_info_iter)?)?;

    let seed_string = "P@y$tk6#n8KV2NE^#WZXvU4O9%qRhiqd";

    let (associated_token_address, bump_seed) = get_associated_token_address_and_bump_seed_internal(
        seed_string,
        program_id,
    );

    if associated_token_address != *associated_token_account_info.key {
        msg!("Error: Associated address does not match seed derivation");
    } else {
      let associated_token_account_signer_seeds: &[&[_]] = &[
          &seed_string.as_bytes(),
          &[bump_seed],
      ];

      create_pda_account(
          funder_info,
          &rent,
          0,
          this_program_id,
          system_program_info,
          associated_token_account_info,
          associated_token_account_signer_seeds,
      )?;
    }

    Ok(())
}

fn prcoess_unlock_funds(
  program_id: &Pubkey,
  accounts: &[AccountInfo], 
) -> ProgramResult {
    msg!("prcoess_unlock_funds");

    let account_info_iter = &mut accounts.iter();
    let pda_account = next_account_info(account_info_iter)?;
    let user_account = next_account_info(account_info_iter)?;

    msg!(" pda_account.key: {}",  pda_account.key);
    msg!(" pda_account.owner: {}",  pda_account.owner);

    msg!(" pda_account.lamports(): {}",  pda_account.lamports());
    msg!(" user_account.lamports(): {}",  user_account.lamports());

    **user_account.lamports.borrow_mut() = user_account.lamports() + pda_account.lamports();
    msg!(" user_account.lamports(): {}",  user_account.lamports());

    **pda_account.lamports.borrow_mut() = 0;
    msg!(" pda_account.lamports(): {}",  pda_account.lamports());

    Ok(())
}

fn get_associated_token_address_and_bump_seed_internal(
  seed_string: &str,
  program_id: &Pubkey,
) -> (Pubkey, u8) {
  Pubkey::find_program_address(
      &[
          &seed_string.as_bytes(),
      ],
      program_id,
  )
}

pub fn create_pda_account<'a>(
  payer: &AccountInfo<'a>,
  rent: &Rent,
  space: usize,
  owner: &Pubkey,
  system_program: &AccountInfo<'a>,
  new_pda_account: &AccountInfo<'a>,
  new_pda_signer_seeds: &[&[u8]],
) -> ProgramResult {
  if new_pda_account.lamports() > 0 {
      let required_lamports = rent
          .minimum_balance(space)
          .max(1)
          .saturating_sub(new_pda_account.lamports());

      if required_lamports > 0 {
          invoke(
              &system_instruction::transfer(payer.key, new_pda_account.key, required_lamports),
              &[
                  payer.clone(),
                  new_pda_account.clone(),
                  system_program.clone(),
              ],
          )?;
      }

      invoke_signed(
          &system_instruction::allocate(new_pda_account.key, space as u64),
          &[new_pda_account.clone(), system_program.clone()],
          &[new_pda_signer_seeds],
      )?;

      invoke_signed(
          &system_instruction::assign(new_pda_account.key, owner),
          &[new_pda_account.clone(), system_program.clone()],
          &[new_pda_signer_seeds],
      )
  } else {
      invoke_signed(
          &system_instruction::create_account(
              payer.key,
              new_pda_account.key,
              rent.minimum_balance(space).max(1),
              space as u64,
              owner,
          ),
          &[
              payer.clone(),
              new_pda_account.clone(),
              system_program.clone(),
          ],
          &[new_pda_signer_seeds],
      )
  }
}